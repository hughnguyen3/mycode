import numpy as np

# CHALLENGE 01 - create a 4-d array using dimensions of your choosing
arr4d = np.random.randint(11, size=(2, 2, 3, 4)) # 4-D array
print("Four-dimensional array:", arr4d, sep="\n")

# CHALLENGE 02 - use array indexing to grab the last value of each array
print("Last element of arr1d", arr1d[-1])
print("Last element of arr2d", arr2d[-1,-1])
print("Last element of arr3d", arr3d[-1,-1,-1])
print("Last element of arr4d", arr4d[-1,-1,-1,-1])

# CHALLENGE 02 - use array indexing to grab the last value of each array
print("Last element of arr1d", arr1d[-1])
print("Last element of arr2d", arr2d[-1,-1])
print("Last element of arr3d", arr3d[-1,-1,-1])
print("Last element of arr4d", arr4d[-1,-1,-1,-1])


