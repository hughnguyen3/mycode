import pandas as pd

# Path to the CSV file
csv_file_path = "~/mycode/challenge/challenge01.txt"

# Read the CSV file and create a dataframe
dataframe = pd.read_csv('annualcheeseconsumed.txt')

# Display the dataframe
print(dataframe)

dataframe_r = dataframe[['Cheddar', 'Swiss', 'Muenster']]
print(dataframe_r)

dataframe_s = dataframe_r.sort_index(ascending=False)
print(dataframe_s)

mean_cheddar = dataframe.loc['1970':'2017', 'Cheddar'].mean()
print(f"Average cheddar consumed from 1970 to 2017: f{mean_cheddar} pounds")

