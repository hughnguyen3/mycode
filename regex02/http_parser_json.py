import requests

#!/usr/bin/python3

import requests

URL= "http://api.open-notify.org/astros.json"
def main():
    # requests.get() sends GET request to the URL
    # .json() strips JSON off the response and translates into Python!
    resp= requests.get(URL).json()

    print("JSON data:")
    print(resp)

    # the API gives us a headcount of people in space
    people_num = resp["number"]
    print();
    print(f"Number of people in Space: {people_num}")
    #print(resp["number"])

    print();
    print(f"Astronaults and spacecrafts:")


    # the value of the key "people" is a list of dictionaries, one dictionary per astronaut
    listofdicts= resp["people"]

    # loop over each dictionary, and print out the values of "name" and "craft" from each one
    for astrodict in listofdicts:
        astro_name = astrodict["name"]
        craft_name = astrodict["craft"]
        print(f"{astro_name} is on the {craft_name}")

main()
