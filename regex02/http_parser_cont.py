#!/usr/bin/env python3
"""
RZFeeser || Alta3 Research
Using regular expression to parse HTTPS responses
"""

import requests
import re

count = 0;

def count_match_strings(text, search_string):
    count = text.count(search_string)
    return count


def main():

    # prompt user for "url" and "searchFor"
    print(f"Welcome to the simple HTTP response parser. Where should we search (ex: https://alta3.com)?")
    url = input()

    while True:

        searchFor = input("Great! So we'll try to open this url {url} to search for the phrase (type 'quit' to exit): ")

        if searchFor.lower() == "quit":
            print(f"Number of matches: {count}")
            print("Exiting the program ...")
            break
                            
        # Process the search string here (you can add your own logic)
        print(f"Searching for '{searchFor}'...")

        # send an HTTP GET to the "url", then strip off the attached HTML data
        searchMe = requests.get(url).text
        
        if re.search(searchFor, searchMe):
            match_count = count_match_strings(searchFor.lower(), searchMe.lower())
            print(f"Found {match_count}  matches!")
        else:
            print("No match!")

if __name__ == "__main__":
    main()
