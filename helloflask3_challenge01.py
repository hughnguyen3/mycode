from flask import Flask

import folium

app = Flask(__name__)

# HTTP GET /
# create a map that returns hanoi 
@app.route('/')
def index():
    start_coords = (21.024, 105.841)
    folium_map = folium.Map(location=start_coords, zoom_start=14)
    return folium_map._repr_html_()

# HTTP GET /namdinh
# create a map that returns a display of namdinh city
@app.route('/namdinh')
def namdinh():
    start_coords = (20.434, 106.177)
    folium_map = folium.Map(location=start_coords, zoom_start=14)
    return folium_map._repr_html_()

if __name__ == '__main__':
    app.run(host="0.0.0.0", port=2224, debug=True)

# Define the coordinates we want our markers to be at
hanoi_coor = [21.0244, 105.8412]     # famous hotel
namdinh_coor = [20.2792, 106.2051]   # family theme park

# Add markers to the map
# popup --> label for the Marker; click on the pins on the map!
folium.Marker(hanoi_coor, popup = 'Ha Noi').add_to(folliumm_map)
folium.Marker(namdinh_coor, popup = 'Nam Dinh').add_to(folium_map)
folium.Marker(comics_and_paperback_plus, popup = 'Comics and Paperback Plus').add_to(my_map)

# change the icon on a marker (location of Hershey Factory)
# see more icons here: https://getbootstrap.com/docs/3.3/components/
#folium.Marker([40.285, -76.650], popup = 'Hershey Factory', icon=folium.Icon(color="red", icon="info-sign")).add_to(my_map)
